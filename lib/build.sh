#!/bin/bash

BUILD_SAMA5(){
	echo "Ainda não implementado"
}

BUILD_MIPS(){
	tar -xf pjproject-2.6.tar.bz2
	mv pjproject-2.6 pjproject-2.6_mips
	cd pjproject-2.6_mips
	./configure --host=mips-openwrt-linux-uclibc --disable-video --disable-ssl --disable-sound --disable-gsm-codec --disable-speex-codec --disable-speex-aec --disable-opencore-amr --disable-libwebrtc
	touch pjlib/include/pj/config_site.h
	make dep
	make
}

BUILD_PC(){
	tar -xf pjproject-2.6.tar.bz2
	mv pjproject-2.6 pjproject-2.6_pc
	cd pjproject-2.6_pc
	./configure --disable-ilbc-codec --disable-speex-codec --disable-speex-aec --disable-gsm-codec --disable-libwebrtc
#	./configure --disable-video --disable-ssl --disable-sound --disable-gsm-codec --disable-speex-codec --disable-speex-aec --disable-opencore-amr --disable-libwebrtc --disable-small-filter --disable-large-filter --disable-l16-codec --disable-g722-codec --disable-g7221-codec --disable-ilbc-codec --disable-libsamplerate --disable-sdl
	touch pjlib/include/pj/config_site.h
	make dep
	make
}



if [ "$1" = "pc" ]
then
	BUILD_PC
elif [ "$1" = "sama5" ]
then
	BUILD_SAMA5
elif [ "$1" = "mips" ]
then
	BUILD_MIPS
else
	echo "Plataforma não suportada"
fi

