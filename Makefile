PLATAFORM:=pc
LIBNAME:=x86_64-unknown-linux-gnu
CC=gcc

INCLUDE:= -I./lib/pjproject-2.6_$(PLATAFORM)/pjlib/include \
	      -I./lib//pjproject-2.6_$(PLATAFORM)/pjlib-util/include \
          -I./lib/pjproject-2.6_$(PLATAFORM)/pjnath/include \
		  -I./lib/pjproject-2.6_$(PLATAFORM)/pjmedia/include \
		  -I./lib/pjproject-2.6_$(PLATAFORM)/pjsip/include \
		  -I./include

#SRC:= src/main.c \
	  src/register.c

SRC:=src/main.c 

SRC_STREAMUTIL:=streamutil/streamutil.c

LIBDIR:= -L./lib//pjproject-2.6_$(PLATAFORM)/pjlib/lib \
		 -L	./lib/pjproject-2.6_$(PLATAFORM)/pjlib-util/lib \
		 -L	./lib/pjproject-2.6_$(PLATAFORM)/pjnath/lib \
		 -L	./lib/pjproject-2.6_$(PLATAFORM)/pjmedia/lib \
		 -L	./lib/pjproject-2.6_$(PLATAFORM)/pjsip/lib \
		 -L	./lib/pjproject-2.6_$(PLATAFORM)/third_party/lib


LIBS:= -lpjsua-$(LIBNAME) \
	   -lpjsip-ua-$(LIBNAME) \
	   -lpjsip-simple-$(LIBNAME) \
	   -lpjsip-$(LIBNAME) \
	   -lpjmedia-codec-$(LIBNAME) \
       -lpjmedia-$(LIBNAME) \
	   -lpjmedia-videodev-$(LIBNAME) \
	   -lpjmedia-audiodev-$(LIBNAME) \
	   -lpjmedia-$(LIBNAME) \
	   -lpjnath-$(LIBNAME) \
	   -lpjlib-util-$(LIBNAME)  \
	   -lpj-$(LIBNAME) \
	   -lsrtp-$(LIBNAME) \
	   -lm \
	   -lrt \
	   -lpthread \
	   -luuid \
	   -lcrypto \
	   -lasound
all:
	$(CC) $(INCLUDE) $(SRC_STREAMUTIL) -o stream $(LIBDIR) $(LIBS)

clean:
	@rm -rf stream

